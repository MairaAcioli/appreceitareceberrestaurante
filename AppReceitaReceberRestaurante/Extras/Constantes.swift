//
//  Constantes.swift
//  AppReceitaReceberRestaurante
//
//  Created by Maíra Preto on 21/10/19.
//  Copyright © 2019 Maíra Acioli. All rights reserved.
//

import Foundation

struct Constantes {
    
    struct Storyboard {
        
        static let bemVindoViewController = "BemVindoViewController"
        
    }
}
